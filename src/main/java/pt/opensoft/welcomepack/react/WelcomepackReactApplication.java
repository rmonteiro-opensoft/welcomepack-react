package pt.opensoft.welcomepack.react;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WelcomepackReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(WelcomepackReactApplication.class, args);
	}

}
