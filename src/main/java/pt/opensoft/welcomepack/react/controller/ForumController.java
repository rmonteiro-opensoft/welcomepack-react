package pt.opensoft.welcomepack.react.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ForumController {


	@GetMapping("/profile")
	public String home(Model model) {

		return "profile"; //view
	}

	@GetMapping("/dummy")
	public String dummy(Model model) {

		return "dummy";  //view
	}


	// /hello?name=kotlin
	@GetMapping("/hello")
	public String mainWithParam( @RequestParam(name = "name", required = false, defaultValue = "") String name, Model model) {

		model.addAttribute("message", name);
		model.addAttribute("test", "11111");

		return "welcome"; //view
	}


}