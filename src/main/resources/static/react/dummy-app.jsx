import React from 'react';
import ReactDOM from 'react-dom';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


export function DummyApp({message, prop2}){


    return (
        <>
            <Row>
                <Col>
                    <div>I am the DUMMY-APP react app (dummy-app) </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <div>This is message: {message}</div>
                </Col>
                <Col>
                    <div>This is prop2: {prop2}</div>
                </Col>
            </Row>

        </>
    )
}



export function startDummyApp(rootElement, portletModel) {
    ReactDOM.render(
        <DummyApp message={portletModel.message}
                  prop2={portletModel.prop2}
        />
        ,
        rootElement)
}

global.startDummyApp = startDummyApp;
