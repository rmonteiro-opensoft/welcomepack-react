const webpack = require('webpack');

module.exports = {
    entry: {
        'dummy-app': './src/main/resources/static/react/dummy-app.jsx'
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [ "@babel/preset-env", "@babel/preset-react" ],
                        plugins: [ "@babel/plugin-transform-arrow-functions", "@babel/plugin-proposal-class-properties" ]
                    }
                }
            }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        path: __dirname + '/src/main/resources/static/bundles',
        publicPath: '/',
        filename: '[name].bundle.js',
        sourceMapFilename: "[name].js.map"
    },
    // Only to use in development
    optimization: {
        minimize: false
    },
    devtool:"eval-source-map",
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
    devServer: {
        contentBase: __dirname + '/src/main/resources/static/bundles',
        hot: true
    }

};